var pathOptions = {
    strokeColor: 'black',
    strokeWidth: 5,
}

var points = new Path();

for (var i = 0; i<1000; i++) {
    var point = Point.random() * new Point(view.viewSize.width,view.viewSize.height);
    points.add(point);
    // var circle = Path.Circle(point, 10);
    // circle.fillColor = 'black';
}

var path = new Path(pathOptions);
var compound = new CompoundPath(pathOptions);
compound.addChild(path);

drawPoints(points.segments[0]);

function drawPoints(segment) {
    var point = segment.point;

    // Prevent intersections
    if (path.lastSegment) {
        var testpath = new Path([path.lastSegment.point, point]);
        if (path.getCrossings(testpath).length != 0) {
            path = new Path(pathOptions);
            compound.addChild(path);
        }
    }

    path.add(point);
    
    // Remove the used segment
    points.removeSegment(segment.index);

    // Loop again
    var nextPoint = points.getNearestPoint(point);
    var nextLocation = points.getLocationOf(nextPoint);
    if (!nextLocation) {
        return
    }
    drawPoints(nextLocation.segment);
}

compound.smooth({type: 'catmull-rom', factor: 0.5});